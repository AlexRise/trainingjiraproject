AJS.toInit(function () {

});

(function ($) {
    function createHeadingPicker(ctx) {
        $(".heading-picker", ctx).each(function () {
            var $this = $(this);
            var control = new JIRA.HeadingPicker({
                element: $this
            });
            $(document).trigger(JIRA.HeadingPicker.READY_EVENT, control);
        });
    }

    JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function (e, context, reason) {
        if (reason !== JIRA.CONTENT_ADDED_REASON.panelRefreshed) {
            createHeadingPicker(context);
        }
    });
})(AJS.$);


JIRA.HeadingPicker = AJS.SingleSelect.extend({

    init: function init(options) {

        var element = options.element;
        var oldHandleFreeInput;
        var self = this;
        var wrmContextPath = require('wrm/context-path');
        var $inputField = $('#hidden-link');

        AJS.$.extend(options, {
            submitInputVal: true,
            showDropdownButton: true,
            removeDuplicates: true,
            ajaxOptions: {
                url: wrmContextPath() + "/rest/testApi/1.0/headings",
                query: true, // keep going back to the server for each keystroke
                data: constructRequestData,
                formatResponse: formatResponse,
                error: handleAjaxError // use our own error callback
            }
        });

        var _parentSetSelection = this.setSelection;
        this.setSelection = function (descriptor, triggerChangeEvent) {
            if (descriptor.properties["customItem"]) {
                return;
            }
            _parentSetSelection.apply(this, arguments);

            if (arguments[0].properties.link != null) {
                $inputField.val(arguments[0].properties.link);
            }
        };

        initEpicPicker();

        /**
         * Returns the data sent to the server for the AJAX search
         *
         * @param query
         * @returns {{searchQuery: *}}
         */
        function constructRequestData(query) {
            return {
                searchQuery: query
            };
        }

        /**
         * The server matches using contains on the epic name and keys.
         * Reflect this with the correct highlighting of matching string.
         * (SingleSelect by default uses startWith matching)
         *
         * @param {string} headingName
         * @param {string} headingLink
         * @param {boolean} isDoneHeading
         * @param {string} searchTerm
         * @returns {string}
         */
        function formatItemLabelHtml(headingName, headingLink, isDoneHeading, searchTerm) {
            // escape regex characters in the searchTerm
            headingName = AJS.escapeHTML(String(headingName));
            searchTerm = AJS.escapeHTML(String(searchTerm));

            var searchTermUC = searchTerm.toUpperCase();
            var headingNameUC = headingName.toUpperCase();
            searchTermUC = (searchTermUC + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
            var searchRegEx = new RegExp("(" + searchTermUC + ")", 'gi');

            var i = headingNameUC.search(searchRegEx);
            var match;
            if (i > -1) {
                match = headingName.slice(i, i + searchTerm.length);
                headingName = headingName.replace(match, "<em>" + match + "</em>");
            }

            return headingName;
        }

        /**
         * Format the response we show after the user types something in the field, i.e. the epics that match
         * their query
         *
         * @param response
         * @returns {Array}
         */

        function formatResponse(response) {
            if (!response || !response.listHeadings || response.total === 0) {
                return [];
            }

            var ret = [];
            var query = self.getQueryVal();
            var returnTotal = 0;

            for (var i = 0; i < response.listHeadings.length; i++) {
                var headingList = response.listHeadings[i];
                var headings = headingList.headings;
                var groupDescriptor = getGroupDescriptor(i + 1, headingList.listDescriptor);

                if (headings.length > 0) {
                    _.each(headings, function (heading) {
                        groupDescriptor.addItem(generateGroupDescriptorItem(heading, query));
                    });
                    ret.push(groupDescriptor);
                }
                returnTotal += headings.length;
            }

            if (returnTotal > 0) {
                var customGroupDescriptor = new AJS.GroupDescriptor({
                    weight: 0, // index of group in dropdown
                    showLabel: false,
                    items: [new AJS.ItemDescriptor({
                        label: query,
                        highlighted: true,
                        styleClass: "ghx-epic-menu-header",
                        customItem: true,
                        html: "<li>" + "<h5>" + AJS.I18n.getText("gh.epic.link.picker.title.description", returnTotal, response.total) + "</h5>" + "<label for='chkShowDoneEpic'>" + "<input type='checkbox' id='chkShowDoneEpic'" + (isShowDone() ? " checked" : "") + ">" + AJS.I18n.getText("gh.epic.link.picker.show.done.epics") + "" + "</label>" + "</li>"
                    })]
                });
                ret.unshift(customGroupDescriptor);
            }

            return ret;
        }

        /**
         *
         * @param heading
         * @param query
         */
        function generateGroupDescriptorItem(heading, query) {
            var headingLink = heading.link;
            var headingName = heading.name;
            var isDoneHeading = heading.isDone;
            var labelHtml = formatItemLabelHtml(headingName, headingName, isDoneHeading, query);
            return new AJS.ItemDescriptor({
                value: headingName,
                fieldText: headingName,
                label: labelHtml,
                link: headingLink,
                html: labelHtml,
                allowDuplicate: false,
                highlighted: true
            });
        }

        /**
         * Generate a group descriptor for displaying the search results.
         *
         * @param {number} index
         * @param {string} label
         * @returns {AJS.GroupDescriptor}
         */
        function getGroupDescriptor(index, label) {
            return new AJS.GroupDescriptor({
                weight: index, // index of group in dropdown
                label: label,
                replace: true // Allow subsequent calls to replace model items,
            });
        }

        /**
         * Override the default error behaviour in Jira to stop an alert from appearing if the ajax fails.
         *
         * @param xhr
         * @param textStatus
         * @param msg
         */
        function handleAjaxError(xhr, textStatus, msg) {
            var $element = AJS.$(element);

            $element.siblings('.ghx-error').remove();
            $element.before(GH.tpl.rapid.notification.renderAuiMessage({
                message: AJS.I18n.getText("gh.error.errortitle") + ': ' + msg,
                type: "error",
                className: "ghx-error aui-ss"
            }));
        }

        /**
         * Validates free input and describes the resulting behaviour to the user.
         * This is called when the input given could not be connected with a unique option from the
         * select menu, and so if the value is non-empty we must show the user an error.
         *
         * @param value
         * @returns {{isError: Boolean, message: String}}
         */
        function validateFreeInput(value) {
            var errorMessage = "";
            var isError = false;
            if (value && value.length > 0) {
                errorMessage = AJS.I18n.getText("gh.epic.link.error.not.found.by.key", value);
                isError = true;
            }
            return {
                isError: isError,
                message: errorMessage
            };
        }

        /**
         * Handle the case where text remains unselected in the text field.
         * This calls the function for handling free input defined in SingleSelect, but adds some functionality
         * afterwards. In the SingleSelect function, an error message is only shown inside of the if statement below
         * when self.options.submitInputVal is false. We want an error to be shown when submitInputVal is true,
         * so that an error is shown before on un-focus and on submit.
         *
         * @param value
         */
        function epicPickerHandleFreeInput(value) {
            var result, descriptor;

            oldHandleFreeInput.apply(self, [value]);
            value = value || AJS.$.trim(self.$field.val());
            descriptor = self.model.getDescriptor(value);
            if (!descriptor && self.$container.hasClass("aui-ss-editing")) {
                result = validateFreeInput(value);
                if (result.isError) {
                    self.options.errorMessage = result.message;
                    self.showErrorMessage(value);
                } else {
                    self.hideErrorMessage();
                }
            }
        }

        /**
         * Submit the EpicPicker form with the current field contents.
         *
         * Note: there is a self.submitForm() function made available in the SingleSelect module,
         * but this has the side-effect of performing a call to handleFreeInput(), which results in
         * an infinite recursion when called from epicPickerHandleFreeInput().
         */
        function submitEpicPickerForm() {
            AJS.$(self.$field[0].form).submit();
        }

        /**
         * Initialise the epic picker module.
         */
        function initEpicPicker() {
            self._super(options);
            oldHandleFreeInput = self.handleFreeInput;
            self.handleFreeInput = epicPickerHandleFreeInput;

            var InlineLayer = require('jira/ajs/layer/inline-layer');
            self.dropdownController.bind(InlineLayer.EVENTS.show, function (event, $dialog) {
                $dialog.off('click', '.ghx-epic-menu-header label[for="chkShowDoneEpic"]', handleClickOnShowDoneCheckbox);
                $dialog.on('click', '.ghx-epic-menu-header label[for="chkShowDoneEpic"]', handleClickOnShowDoneCheckbox);
            });
        }

        function isShowDone() {
            return localStorage.getItem("gh.epicpicker.showdone") === "true";
        }

        function handleClickOnShowDoneCheckbox(event) {
            event.preventDefault();

            var showDoneCheckboxState = !isShowDone();
            localStorage.setItem("gh.epicpicker.showdone", showDoneCheckboxState);
            // reload epics with new status filter
            self.$field.click();
            return false;
        }
    }

});

JIRA.HeadingPicker.READY_EVENT = "gh.epic-picker.ready";