package by.mik.jira.trainingProject.web.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class ListHeadings {

    @XmlElement
    public final List<Headings> listHeadings = new ArrayList<>();
}
