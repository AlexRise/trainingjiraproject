package by.mik.jira.trainingProject.web.api;

import by.mik.jira.trainingProject.web.model.Heading;
import by.mik.jira.trainingProject.web.model.Headings;
import by.mik.jira.trainingProject.web.model.ListHeadings;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.ArrayList;
import java.util.List;

@Path("/headings")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class RestHeadingService {

    private List<Heading> content = new ArrayList<>();

    public RestHeadingService() {
        content.add(new Heading("People", "https://people.onliner.by"));
        content.add(new Heading("Opinions", "https://people.onliner.by/opinions"));
        content.add(new Heading("Auto", "https://auto.onliner.by/"));
        content.add(new Heading("Tech", "https://tech.onliner.by/"));
        content.add(new Heading("Realt", "https://realt.onliner.by/"));
    }

    @GET
    public Response listHeadings(@QueryParam("searchQuery") final String searchQuery) {

        ListHeadings listHeadings = new ListHeadings();
        Headings headings = new Headings("All Result");
        filterContent(headings, searchQuery);
        listHeadings.listHeadings.add(headings);

        return Response.ok(listHeadings).build();
    }

    private void filterContent(Headings headings, String searchQuery) {
        for (Heading heading : content) {
            if (heading.getName().toLowerCase().contains(searchQuery.toLowerCase())) {
                headings.headings.add(heading);
            }
        }
    }
}
