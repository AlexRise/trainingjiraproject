package by.mik.jira.trainingProject.api;

public interface MyPluginComponent
{
    String getName();
}