package by.mik.jira.trainingProject.customFields;


import by.mik.jira.trainingProject.model.Heading;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.customfields.impl.AbstractCustomFieldType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.rest.RestCustomFieldTypeOperations;
import com.atlassian.jira.issue.fields.rest.RestFieldOperationsHandler;
import com.atlassian.jira.issue.fields.rest.json.JsonData;

import com.atlassian.jira.util.ErrorCollection;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.Gson;

import java.util.*;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.google.common.collect.Iterables.getLast;
import static java.util.Collections.singletonList;


@Scanned
public class HeadingCFType
        extends AbstractCustomFieldType<Heading, String>
        implements RestCustomFieldTypeOperations {

    private static final String KEY_LINK = "link";
    private static final String KEY_NAME = "name";

    @ComponentImport
    private final CustomFieldValuePersister customFieldValuePersister;
    @ComponentImport
    private final GenericConfigManager genericConfigManager;
    private Gson gson = new Gson();

    public HeadingCFType(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
        this.customFieldValuePersister = customFieldValuePersister;
        this.genericConfigManager = genericConfigManager;
    }

    @Nullable
    public String getSingularObjectFromString(@Nullable String string) {
        return string;
    }

    @Override
    public Set<Long> remove(CustomField field) {
        return Sets.newHashSet();
    }

    public void validateFromParams(CustomFieldParams relevantParams, ErrorCollection errorCollectionToAddTo, FieldConfig config) {

    }

    @Override
    public void createValue(CustomField customField, Issue issue, @Nonnull Heading heading) {
        customFieldValuePersister.createValues(customField, issue.getId(), getDatabaseType(), Lists.newArrayList(convertHeadingToJson(heading)));
    }

    private String convertHeadingToJson(Heading heading) {
        return gson.toJson(heading);
    }

    @Override
    public void updateValue(CustomField customField, Issue issue, Heading heading) {
        customFieldValuePersister.updateValues(customField, issue.getId(), getDatabaseType(), Lists.newArrayList(convertHeadingToJson(heading)));
    }

    public Heading getValueFromCustomFieldParams(CustomFieldParams parameters) {
        return getValueCustomFieldParams(parameters);
    }

    public Object getStringValueFromCustomFieldParams(CustomFieldParams parameters) {
        return parameters.getFirstValueForNullKey();
    }

    @Nullable
    public Heading getValueFromIssue(CustomField field, Issue issue) {
        final List<Object> values = customFieldValuePersister.getValues(field, issue.getId(), getDatabaseType());

        final Object databaseValue;
        if (values.isEmpty()) {
            return null;
        } else if (values.size() > 1) {
            databaseValue = getLast(values);
            customFieldValuePersister.updateValues(field, issue.getId(), getDatabaseType(), singletonList(databaseValue));
        } else {
            databaseValue = values.get(0);
        }

        if (databaseValue == null) {
            return null;
        }
        try {
            return getObjectFromDbValue(databaseValue);
        } catch (FieldValidationException e) {
            return null;
        }
    }

    private Heading getObjectFromDbValue(@Nonnull final Object databaseValue) throws FieldValidationException {
        return gson.fromJson((String) databaseValue, Heading.class);
    }

    public Heading getDefaultValue(FieldConfig fieldConfig) {
        return null;
    }

    @Override
    public void setDefaultValue(FieldConfig fieldConfig, Heading heading) {
        genericConfigManager.update(DEFAULT_VALUE_TYPE, fieldConfig.getId().toString(), heading);
    }

    @Nullable
    @Override
    public String getChangelogValue(CustomField customField, Heading heading) {
        return null;
    }

    @Nullable
    private Heading getValueCustomFieldParams(CustomFieldParams relevantParams) {

        if (relevantParams.getValuesForKey(KEY_LINK) == null || relevantParams.getValuesForKey(KEY_NAME) == null) {
            return null;
        }

        List<String> links = new ArrayList<>(relevantParams.getValuesForKey(KEY_LINK));
        List<String> names = new ArrayList<>(relevantParams.getValuesForKey(KEY_NAME));

        return new Heading(links.get(0), names.get(0));
    }

    public RestFieldOperationsHandler getRestFieldOperation(CustomField field) {
        return null;
    }

    public JsonData getJsonDefaultValue(IssueContext issueCtx, CustomField field) {
        return null;
    }

    @Override
    public String getStringFromSingularObject(String string) {
        return StringUtils.defaultString(string);
    }

    @Nonnull
    private PersistenceFieldType getDatabaseType() {
        return PersistenceFieldType.TYPE_UNLIMITED_TEXT;
    }
}
