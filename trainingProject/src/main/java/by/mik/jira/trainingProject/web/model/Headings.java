package by.mik.jira.trainingProject.web.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Headings {

    @XmlElement
    private String listDescriptor;

    @XmlElement
    public final List<Heading> headings = new ArrayList<>();

    public Headings(String listDescriptor) {
        this.listDescriptor = listDescriptor;
    }

    public String getListDescriptor() {
        return listDescriptor;
    }

    public void setListDescriptor(String listDescriptor) {
        this.listDescriptor = listDescriptor;
    }
}
