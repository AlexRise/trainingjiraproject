package ut.by.mik.jira.trainingProject;

import org.junit.Test;
import by.mik.jira.trainingProject.api.MyPluginComponent;
import by.mik.jira.trainingProject.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}